# Stage 1: node builder
FROM node:10.16.0-alpine as node
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install && mkdir /ng-app && mv ./node_modules /ng-app/
WORKDIR /ng-app
COPY . .
ARG version=production
RUN npm run ng build -- -c $version --output-path=dist

# Stage 2: nginx:apline runner
FROM nginx:alpine
# support running as arbitrary user which belogs to the root group
RUN chmod g+rwx /var/cache/nginx /var/run /var/log/nginx
# change listen port in default conf
RUN sed -i.bak 's/listen\(.*\)80;/listen 8080;/' /etc/nginx/conf.d/default.conf
# fix 404 error when load page with path in default conf
RUN sed -i.bak 's/index\(.*\)index.html\(.*\)index.htm;/try_files $uri $uri\/ \/index.html;/' /etc/nginx/conf.d/default.conf
EXPOSE 8080
# comment user directive as master process is run as user in OpenShift anyhow
RUN sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf

# --- if you have to change more in default.conf and dont like sed         --- #
# --- just prepare nginx/default.conf by yourserf and uncomment line below --- #
# COPY nginx/default.conf /etc/nginx/conf.d/default.conf

# clean nginx file serving dir
RUN rm -rf /usr/share/nginx/html*
# copy production files into mentioned dir
COPY --from=node /ng-app/dist /usr/share/nginx/html
